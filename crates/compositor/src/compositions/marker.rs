use skia_safe::{canvas::Canvas, Color, Paint, Path};

pub fn render_marker(size: f32, canvas: &mut Canvas) {
    let center = size / 2.0;
    let arrow_size = size / 3.2;
    let arrow_offset = arrow_size / 10.0;
    let radius = (size - arrow_size) / 2.0;

    let mut paint = Paint::default();
    paint.set_anti_alias(true);
    paint.set_stroke_width(1.0);
    paint.set_style(skia_safe::PaintStyle::Fill);

    let mut path_outer_circle = Path::new();
    paint.set_color(Color::RED);
    path_outer_circle.add_circle((center, center), radius, None);
    canvas.draw_path(&path_outer_circle, &paint);

    let mut path_arrow = Path::new();
    paint.set_color(Color::RED);
    path_arrow.move_to((center, 0.0));
    path_arrow.line_to((center - arrow_size, arrow_size + arrow_offset));
    path_arrow.line_to((center + arrow_size, arrow_size + arrow_offset));
    path_arrow.close();
    canvas.draw_path(&path_arrow, &paint);

    let mut path_inner_circle = Path::new();
    paint.set_color(Color::BLACK);
    path_inner_circle.add_circle((center, center), radius - 3.0, None);
    canvas.draw_path(&path_inner_circle, &paint);
}
