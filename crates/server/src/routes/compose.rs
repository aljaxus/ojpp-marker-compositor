use axum::{extract::Query, response::IntoResponse};
use serde::{Serialize, Deserialize};
use compositor::compositions::render_marker;
use http::{header, StatusCode};
use skia_safe::{EncodedImageFormat, Surface};

#[derive(Serialize, Deserialize)]
pub struct ComposeQuery {
    pub size: i32,
}

pub async fn compose(query: Query<ComposeQuery>) -> impl IntoResponse {
    let size = query.size;

    let mut surface =
        Surface::new_raster_n32_premul((size, size)).expect("No SKIA surface available.");

    render_marker(size as f32, surface.canvas());

    let image = surface.image_snapshot();
    let encoded_image = match image.encode_to_data(EncodedImageFormat::PNG) {
        Some(data) => data.as_bytes().to_owned(),
        None => {
            return (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to encode image to PNG.",
            )
                .into_response()
        }
    };

    (
        axum::response::AppendHeaders([
            (header::CONTENT_TYPE, "image/png"),
            (header::CACHE_CONTROL, "public, max-age=31536000"),
        ]),
        encoded_image,
    )
        .into_response()
}
