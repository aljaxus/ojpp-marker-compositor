use std::net::TcpListener;

use axum::{routing::get, Router};
use axum_server::Handle;
use clap::Parser;
use futures::future::BoxFuture;
use futures::FutureExt;
use http::Request;
use hyper::Body;
use server::{openapi::APIDoc, routes::compose, state::ServerState};
use tower::{make::Shared, steer::Steer, BoxError, ServiceExt};
use tower_http::trace::TraceLayer;
use tracing::{
    middleware::RequestIdLayer,
    tracing::{get_subscriber, init_subscriber, TowerMakeSpanWithConstantId},
};
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

fn server(listener: TcpListener) -> BoxFuture<'static, Result<(), std::io::Error>> {
    let state = ServerState {};

    let http_server = Router::new() //
        .route("/compose/marker", get(compose))
        .merge(SwaggerUi::new("/docs").url("/docs/openapi.json", APIDoc::openapi()))
        // Layers/middleware
        .layer(TraceLayer::new_for_http().make_span_with(TowerMakeSpanWithConstantId))
        .layer(RequestIdLayer)
        .with_state(state)
        .map_err(BoxError::from)
        .boxed_clone();

    let http = Steer::new(vec![http_server], |_req: &Request<Body>, _svcs: &[_]| 0);
    let handle = Handle::new();

    axum_server::from_tcp(listener)
        .handle(handle)
        .serve(Shared::new(http))
        .boxed()
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let subscriber = get_subscriber(
        "ojpp-marker-compositor-server".into(),
        "info,tower_http=trace".into(),
        std::io::stdout,
    );
    init_subscriber(subscriber);

    let args = server::arguments::Arguments::parse();
    tracing::info!("running server with validated arguments:\n{}", args);

    let listener = TcpListener::bind(args.bind_address)?;

    server(listener).await?;

    Ok(())
}
